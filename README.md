# Proyecto TFG Java EE -- BookStore

## Repositorio Infraestructura: **ms-infra**
En este repo se utilizará _Docker-compose_ para levantar el sistema de contenedores de Docker que necesita el proyecto. Utilizaremos el fichero docker-compose.yml que nos permite levantar n servicios/contenedores de Docker a través de comandos.

El proyecto se compondrá de un repositorio que contiene el Microservicio de `BookStore` con la lógica de backend y frontend. 

```
tfg-bookstore
```

Proyecto backend construido a partir de Spring Boot y conjuntamente con un frontend desarrollo con Thymeleaf.


Se debera clonar en la misma raiz que se clonará el repositorio de infra. Esto hará que al ejecutar el docker compose, encuentre el código de Front y Back y genere el contenedor con el proyecto montando con toda una estructura.

```bash
docker-compose up --build
```

En este caso levantaremos los siguientes servicios:

```db:
    image: postgres
    ports:
      - 54320:5432

  adminer:
    image: adminer
    ports:
      - 18080:8080
```

Necesarios para crear y tener la base de datos del proyecto, y su gestión UI.

Por otro lado, levantaremos nuestros proyectos como servicios:

```yml
  backend:
    build:
      context: ../tfg-bookstore
      dockerfile: Dockerfile
    ports:
      - 8080:8080
```

Además se han definidos _healthcheck_ que nos permiten controlar que los servicios dependientes de otros, esperen hasta que los mismos esten activos en caso de haber alguna demora al levantarse.
